@extends('adminlte.master')

@section('title')
Cast ID = {{$cast->id}}
@endsection

@section('content')
<h4>Name: {{$cast->name}}</h4>
<h5>Age: {{$cast->age}}</h5>
<p>Bio: {{$cast->bio}}</p>
@endsection