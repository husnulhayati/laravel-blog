<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; //untuk menggunakan database

class CastController extends Controller
{
    
    //menampilkan form untuk membuat data pemain film baru
    public function create(){
        return view('casts.create');
    }

    //STORE menyimpan data baru ke tabel Cast
    
    public function store(Request $request){
        // dd($request->all()); //untuk memeriksa data yang tersimpan apa saja
       
        $request->validate([
            'name' => 'required|unique:casts,name', //tidak boleh sama, harus unik
            'age' => 'required|integer' //tidak boleh kosong
            ],
            [
            'name.required' => 'The Name field is required',
            'name.unique' => 'The Name is already used',
            'age.required' => 'The Age field is required',
            'age.integer' => 'The Age must be an integer. For example 35'
            
        ]); //untuk memvalidasi inputan

        // dd($request->all());

        DB::table('casts')->insert([
            "name" => $request["name"],
            "age" => $request["age"],
            "bio" => $request["bio"],
        ]);
        return redirect('/cast');

    }

    //menampilkan list data para pemain film*
    public function index()
    {
        $cast = DB::table('casts')->get();
        return view('casts.index', compact('cast'));
    }


    //menampilkan detail data pemain film dengan id tertentu
    public function show($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('casts.show', compact('cast'));
    }

    //menampilkan form untuk edit pemain film dengan id tertentu
    public function edit($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('casts.edit', compact('cast'));
    }

    //menyimpan perubahan data pemain film (update) untuk id tertentu
    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|unique:casts,name', //tidak boleh sama, harus unik
            'age' => 'required|integer' //tidak boleh kosong
            ],
            [
            'name.required' => 'The Name field is required',
            'name.unique' => 'The Name is already used',
            'age.required' => 'The Age field is required',
            'age.integer' => 'The Age must be an integer. For example 35'
            
        ]); //untuk memvalidasi inputan

        DB::table('casts')
            ->where('id', $id)
            ->update([
            'name' => $request["name"],
            'age' => $request["age"],
            'bio' => $request["bio"],
            ]);
        return redirect('/cast');
    }

    //menghapus data pemain film dengan id tertentu
    
    public function erase($id) //izin coba-coba elaborasi, kak
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('casts.delete', compact('cast'));
    }

    public function destroy($id)
    {
        DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
