@extends('adminlte.master')

@section('title')
    Add Cast Data
@endsection

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Input Data Here</h3>
    </div>

    <div class="card-body">
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="string" class="form-control" name="name" id="name" placeholder="Input Name">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="age">Age</label>
                <input type="integer" class="form-control" name="age" id="age" placeholder="Input Age">
                @error('age')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea name="bio" class="form-control" cols="30" rows="10" name="bio" id="bio" placeholder="Input Bio (optional)"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>  
    </div>
@endsection