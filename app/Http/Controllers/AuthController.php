<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata(){
        return view('latihan.auth.register');
    }

    public function signup(Request $request){
        $namadepan = $request["namadepan"];
        $namabelakang = $request["namabelakang"];
        
        return view('latihan.auth.selamat', compact('namadepan','namabelakang'));
    }
}
