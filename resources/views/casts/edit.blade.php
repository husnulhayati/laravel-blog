@extends('adminlte.master')

@section('title')
Edit Cast {{$cast->id}} - {{$cast->name}}
@endsection

@section('content')
    
<div>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Name</label>
            <input type="string" value="{{$cast->name}}" class="form-control" name="name" id="name" placeholder="Input Name">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="age">Age</label>
            <input type="integer" value="{{$cast->age}}" class="form-control" name="age" id="age" placeholder="Input Age">
            @error('age')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea name="bio" class="form-control" cols="30" rows="10" name="bio" id="bio" placeholder="Input Bio (optional)"> {{$cast->bio}} </textarea>
        </div>
        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>

@endsection