@extends('adminlte.master')

@section('title')
Delete Cast {{$cast->id}} - {{$cast->name}}
@endsection

@section('content')
    
<div>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('DELETE')
        <div class="form-group">
            <label for="name">Name</label>
            <input type="string" value="{{$cast->name}}" class="form-control" name="name" id="name">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="age">Age</label>
            <input type="integer" value="{{$cast->age}}" class="form-control" name="age" id="age">
            @error('age')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea name="bio" class="form-control" cols="30" rows="10" name="bio" id="bio"> {{$cast->bio}} </textarea>
        </div>
        <div class="form-group mb-0">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
              <label class="custom-control-label" for="exampleCheck1">I am sure to delete this data</a></label>
            </div>
          </div>
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>
</div>

@endsection